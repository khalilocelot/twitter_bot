<?php
    require_once 'GrammarTables.php';
    require_once 'codebird\Codebird.php';

    class LineConstructor{

        private $is_plural = false;

        /*
        *   Kick starts the recursion function below, give it some imports
        *
        *   @param  arr - the array which contains the different line formats
        *
        *   @return NA
        */
        public function recursionStart($arr){
            $ret_str = "";

            //$this->recursion($arr[rand(0, count($arr) - 1)], $ret_str);
            $this->recursion($arr[rand(0, sizeof($arr) - 1)], $ret_str, (new GrammarTables()));
        }

        /*
        *   Recursively brakes down the any inner format-lines, anything within () in the bnf file
        *
        *   @param  line        - the selected above that will be used as a format to produce a line
        *   @param  ret_str     - the string to be appended to and returned
        *   @param  gr_t        - an object of GrammarTables class
        *
        *   @return ret_str     - the string formed by following the format
        */
        private function recursion($line, $ret_str, $gr_t){
            //splits the line by whitespace between words except within brackets and
            //places them in an array
            $s_arr = preg_split("/\(\s\);?|\{\s\}|\[\s\]|[\s\c?]+(?![^{]*\})/", $line);

            //for every word in the line
            foreach($s_arr as $value){
                $val_len = strlen($value);

                //if the element has two ways of being implemented split down the slash and
                //either select the first or last implementation to work with
                if(strpos($value, '/')){
                    $value = (($ran_num = rand(0, 1)) == 0 ? explode('/', $value)[$ran_num] . ')' : '(' . explode('/', $value)[$ran_num]);
                    $val_len = strlen($value);
                }
                else{
                    $value = $value;
                }

                //if there is a '+' in front of the word then the sentence must be written in
                //plural form from then until the end of the sentence or until it finds a '*';
                if(strpos($value, '+') !== FALSE){
                    $this->is_plural = true;
                }
                elseif(strpos($value, '*') !== FALSE){
                    $this->is_plural = false;
                }

                //if the element is surrounded in curly brackets
                if($value[0] == "{" && $value[$val_len - 1] == "}"){
                    try{
                        $ret_str = $ret_str . " " . $gr_t->parseGrammar((new StringManipulator())->rm_from_enclose($value), $this->is_plural);
                    }catch(Exception $e){echo "EXCEPTION: " . $e->getMessage() . "<br />";}
                }
                //if the element is surround by normal parentheses
                elseif($value[0] == "(" && $value[$val_len - 1] == ")"){
                    //recursively brake down the elements within the parens
                    $ret_str = $this->recursion((new StringManipulator())->rm_from_enclose($value), $ret_str, $gr_t);
                }
                //if the element is surrounded by square brackets
                elseif($value[0] == "[" && $value[$val_len - 1] == "]"){
                    //it's an integer block, shove a random integer at the end
                    $rand_num = rand(0, 100);
                    $ret_str = $ret_str . " " . $rand_num;
                    $this->is_plural = (($rand_num > 1) ? true : false);
                }
                //if the elememnt is surrounded by sharp brackets
                elseif($value[0] == "<" && $value[$val_len - 1] == ">"){
                    $ret_str = $ret_str . " " . ($a = ["Who", "What", "When", "Where", "Why"])[rand(0, 2)];
                }
                //if the element isn't bracketed
                else{
                    //append the element to the back of the return line
                    if($value[0] == '*' || $value[0] == '+')
                        $value = str_replace($value[0], '', $value[0]);
                    $ret_str = $ret_str . " " . $value;
                }

                echo "Ret_Str: " . $ret_str . "<br />";
            }

            \Codebird\Codebird::setConsumerKey("C9XOqOYaeOj8gTfcqKtmj0i4E", "oZ4khEtAy0C1EB4CckPP6UYZDEMVHhqI74XDWYS3Y3fxI14zUz");
            $cb = \Codebird\Codebird::getInstance();
            $cb->setToken("848257813637849088-DGxOExu1haA49prSFU18lBUok66apKx", "BfA6qeP4c30fmNTtvSMOCkfu26ZT3xjuTo6oFEV65Ikpl");

            $params = array(
                'status' => $ret_str
            );
            $reply = $cb->statuses_update($params);

            return $ret_str;
        }
    }
?>
