<?php
    require 'DatabaseDest.php';
    require 'LineConstructor.php';

    $filename_w = '..\\..\\resources\\words.txt';
    $filename_b = '..\\..\\resources\\format_bnf.txt';

    $dest = new DatabaseDest();
    $line_cons = new LineConstructor();

    /*$file_s = fopen($filename_w, "r");

    if(!$file_s){
        echo "The file could not be opened <br />";
        exit();
    }

    while(($line = trim(fgets($file_s))) != null){
        $line_arr = explode('|', $line);
        $dest->categorize($line_arr);
    }

    fclose($file_s);*/

    $file_s = fopen($filename_b, "r");

    if(!$file_s){
        echo "This file could not be opened <br />";
        exit();
    }

    $bnf_array = array();

    while(($line = trim(fgets($file_s))) != null){
        if($line[0] !== '#' && $line[0] !== '')
            array_push($bnf_array, $line);
    }
    fclose($file_s);

    $time_start = microtime(true);

    set_time_limit(0);

    while(true){
        $line_cons->recursionStart($bnf_array);
        sleep(rand(60, 3600));
    }

    echo "The time length of the execution was: " . round((microtime(true) - $time_start) * 1000, 2) . "ms <br />";
?>
