<?php
/*
* Created by Ali Khalil
* Date: 12/02/2017
*/
    require 'EntryValidation.php';

    class DatabaseDest{
        private $word_category;
        private $q_sender;

        public function __construct(){
            $this->q_sender = new EntryValidation();
            $word_category = null;
        }

        public function categorize($line_arr){
            //Check the first element of the array, the letter that indicates the table it belongs to
            switch($line_arr[0]){
                case 'C':
                    $word_category = "Common_Nouns";
                    break;
                case 'Q':
                    $word_category = "Descriptive_Insults";
                    break;
                case 'D':
                    $word_category = "Descriptives";
                    break;
                case 'E':
                    $word_category = "Emotional_Reactions";
                    break;
                case 'P':
                    $word_category = "Prepositions";
                    break;
                case 'O':
                    $word_category = "Proper_Nouns";
                    break;
                case 'V':
                    $word_category = "Verbs";
                    break;
                case 'U':
                    $word_category = "Units_of_Time";
                    break;
                case 'T':
                    $word_category = "Types_of_People";
                    break;
                case 'A':
                    $word_category = "Adverbs";
                    break;
                default:
                    $word_category = "N/A";
                    break;
            }

            //If the line actually belongs to a category...
            if($word_category != "N/A"){
                try{
                    $this->q_sender->insertEntry($line_arr, $word_category);
                }catch(Exception $e){echo $e->getMessage() . "<br />";}
            }
            else
                echo "An error has occurred: please ensure that the letter used to identify the category of the line is valid";
        }
    }
?>
