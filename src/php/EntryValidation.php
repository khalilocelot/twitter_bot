<?php
    /*
    * Created by Ali Khalil
    * Date: 13/02/2017
    */

    require 'Database.php';
    require 'StringManipulator.php';

    class EntryValidation extends DB{
        private $str_man;

        /*
        * Purpose:  Validates and inserts an entry into the database
        * Imports:  line_arr - The array, split up into an array by |, whose
                    information is to placed into the database
                    word_category - The table to have the line inserted into
        * Exports:  An boolean confirming whether or not the entry has been
                    successfully inserted
        */
        public function insertEntry($line_arr, $word_category){
            $time_start = microtime(true);
            $valid_to_insert = null;
            $tablename = "`" . str_replace("_", " ", $word_category) . "`";

            if(count($line_arr) <= 1)
                throw new Exception("Make sure at least the word to place into the database is placed into the line");

            //select the id of the same word in the database as the subject line
            $res = $this->db->query("SELECT ID FROM Words WHERE Word = '$line_arr[1]'");

            //If the word exists in the database and it retrieves the ID
            if($res->num_rows > 0)
                $id = $res->fetch_object()->ID;

            //if the word doesn't exist at all then place it into the Words table
            //and retrieve the ID from the insertion auto increment
            elseif($res->num_rows == 0){
                $this->db->query("INSERT INTO Words (Word) VALUES ('$line_arr[1]')");
                $id = $this->db->insert_id;
            }

            $str_man = new StringManipulator();

            //set the boolean in the database that corresponds with the subject's category to true
            $query_u = "UPDATE Words SET $word_category = 1 WHERE ID = $id";

            //if the query fails, it's a duplicate entry
            if(!$this->db->query($query_u))
                throw new Exception("Duplicate entry of " . $line_arr[1] . "<br />");

            //query to acquire the column names of the corresponding table
            $query_c = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'Word Formats' AND TABLE_NAME = '$tablename'";
            $res = $this->db->query($query_c);

            //place the column names into an array
            $arr = array();
            $arr_count = 0;
            while($x = mysqli_fetch_array($res)){
                $arr[$arr_count] = $x["column_name"];
                $arr_count++;
            }

            //remove the letter and subject word elements from the array
            //not needed as the Words table contains the word and the letter
            //is simply there for initial identification purpose
            array_splice($line_arr, 0, 2);

            //surround the elements of the array containing the information to be placed
            //into the array with quotes, to indicate strings in the MYSQL query
            for($i = 0; $i < count($line_arr); $i++){
                $line_arr[$i] = "'" . $line_arr[$i] . "'";
            }

            //the ID will always be inserted; it's a constant throughout the database
            $to_insert = $id;
            $str_column_names = "";

            //if there is more to to the line to be inserted than just the ID, place commas
            //next to each piece of information
            try{
                var_dump($line_arr);
                if(count($line_arr) >= 1)
                    $to_insert = $to_insert . ", " . $str_man->arr_append($line_arr, ", ");

                var_dump($to_insert);

                //place commas next to each column name
                $str_column_names = $str_man->arr_append($arr, ", ");
            }catch(Exception $e){echo $e->getMessage() . "<br />";}

            //query to insert into the table
            $this->db->query("INSERT INTO $tablename ($str_column_names) VALUES ($to_insert)");
            echo "Category Insertion Error: " . mysqli_error($this->db) . "<br />";

            echo "<br /> Total execution time of the script was: " . round((microtime(true) - $time_start) * 1000, 2) . "ms <br />";
        }
    }
?>
