<?php
    require_once 'Database.php';
    require_once 'StringManipulator.php';

    class GrammarTables extends DB{
        var $is_plural;

        public function parseGrammar($lbl, $is_pl){
            $word;

            $this->is_plural = $is_pl;

            $sub_str = (preg_match('/^[a-zA-Z]/', $lbl[0]) ? $lbl : substr($lbl, 1, strlen($lbl)));

            switch($sub_str){
                case "Common Noun":
                    $word = $this->CommonNouns($lbl);
                    break;
                case "Proper Noun":
                    $word = $this->ProperNouns($lbl);
                    break;
                case "Verb":
                    $word = $this->Verbs($lbl);
                    break;
                case "Preposition":
                    $word = $this->Prepositions($lbl);
                    break;
                case "Descriptive":
                    $word = $this->Descriptives($lbl);
                    break;
                case "Descriptive Insult":
                    $word = $this->DescriptiveInsults($lbl);
                    break;
                case "Type of Person":
                    $word = $this->TypesOfPeople($lbl);
                    break;
                case "Units of Time":
                    $word = $this->UnitsOfTime($lbl);
                    break;
                case "Emotional Reactions":
                case "Descriptive Insults":
                case "Adverbs":
                    $word = $this->getDBWord("`$lbl`")["Word"];
                    break;
                default:
                    throw new Exception("Invalid Label");
            }

            return $word;
        }

        /*
        *   Calls DB for random `Common Nouns` word
        *
        *   @param  lbl     - the name of the table to retrieve from
        *
        *   @return cn_info - the word, either pluralized or in a singular format
        */
        private function CommonNouns($lbl){
            $cn_info = $this->getDBWord("`Common Nouns`");
            return (!$this->is_plural ? $cn_info["Word"] : $this->checkAndPluralize($cn_info["Word"], $cn_info["Pluralization_Method"]));
        }

        /*
        *   Calls DB for random `Unit of Time` word
        *
        *   @param  lbl     - the name of the table to retrieve from
        *
        *   @return cn_info - the word, either pluralized or in a singular format
        */
        private function UnitsOfTime($lbl){
            $cn_info = $this->getDBWord("`Units of Time`");

            return (!$this->is_plural ? $cn_info["Word"] : $this->checkAndPluralize($cn_info["Word"], $cn_info["Pluralization_Method"]));
        }

        /*
        *   Calls DB for random `Verbs` word
        *
        *   @param  lbl      - the name of the table to retrieve from
        *
        *   @return ret_word - the word, either in past tense or present tense
        */
        private function Verbs($lbl){
            $ret_word = "";
            $cn_info = $this->getDBWord("`Verbs`");

            switch ($lbl[0]) {
                case '<':
                    $tense_changer = $cn_info["Past_Tense"];
                    $ret_word = $this->checkAndPluralize($cn_info["Word"], $tense_changer);
                    break;
                case '>':
                    $tense_changer = $cn_info["Present_Tense"];
                    $ret_word = $this->checkAndPluralize($cn_info["Word"], $tense_changer);
                    break;
                case '^':
                    $ret_word = $cn_info["Word"];
                    break;
                default:
                    throw new Exception("Invalid Prepended Character: " . $lbl[0]);
            }

            return $ret_word;
        }

        /*
        *   Calls DB for random `Prepositions` word
        *
        *   @param  lbl     - the name of the table to retrieve from
        *
        *   @return cn_info - the word fetched from the database
        */
        private function Prepositions($lbl){
            $ret_word = "";
            $dml_sub_q = "(SELECT * from ";
            $tbl_name = "`Prepositions` ";
            $where_clause = "where Preposition_Type = ";

            switch ($lbl[0]){
                case '_':
                    $where_clause = $where_clause . " 'Time')";
                    break;
                case ',':
                    $where_clause = $where_clause . " 'Agent')";
                    break;
                case '.':
                    $where_clause = $where_clause . " 'Place')";
                    break;
                case '`':
                    $where_clause = $where_clause . " 'Direction')";
                    break;
                default:
                    throw new Exception("Invalid Prepending Character for Prepositions");
            }

            $where_clause = $where_clause . " as Preposition_Subtype";

            return $this->getDBWord($tbl_name, $dml_sub_q, $where_clause)["Word"];
        }

        /*
        *   Calls DB for random `Proper Nouns` word
        *
        *   @param  lbl     - the name of the table to retrieve from
        *
        *   @return cn_info - the word fetched from the database
        */
        private function ProperNouns($lbl){
            $ret_word = "";
            $dml_sub_q = "(SELECT * from ";
            $tbl_name = "`Proper Nouns` ";
            $where_clause = "where Proper_Noun_Type = ";

            switch ($lbl[0]) {
                case '!':
                    $where_clause = $where_clause . "'Name')";
                    break;
                case '?':
                    $where_clause = $where_clause . "'Place')";
                    break;
                default:
                    throw new Exception("Invalid Prepending Character for Proper Nouns");
                    break;
            }

            $where_clause = $where_clause . " as ProperNoun_Subtype";

            return $this->getDBWord($tbl_name, $dml_sub_q, $where_clause)["Word"];
        }

        /*
        *   Calls DB for random `Descriptives` word
        *
        *   @param  lbl     - the name of the table to retrieve from
        *
        *   @return cn_info["Word"] - the word fetched from the database
        */
        private function Descriptives($lbl){
            $ret_word = "";
            $dml_sub_q = "(SELECT * from ";
            $tbl_name = "Descriptives ";
            $where_clause = "where Descriptive_Type = ";

            switch($lbl[0]){
                case '=':
                    $where_clause = $where_clause . "'Attributes')";
                    break;
                case '&':
                    $where_clause = $where_clause . "'Demonstrative')";
                    break;
                case '%':
                    $where_clause . $where_clause . "'Articles')";
                    break;
                case '@':
                    $where_clause . $where_clause . "'Possessive')";
                    break;
            }

            $where_clause = $where_clause . " as Descriptive_Subtype";

            return $this->getDBWord($tbl_name, $dml_sub_q, $where_clause)["Word"];
        }

        private function DescriptiveInsults($lbl){}

        /*
        *   Calls DB for random `TypesOfPeople` word
        *
        *   @param  lbl     - the name of the table to retrieve from
        *
        *   @return cn_info - the word fetched from the database
        */
        private function TypesOfPeople($lbl){

        }

        /*
        *   Retrieves the information of a random word from the selected database table
        *
        *   @param  tbl_name -      the name of the table to retrieve from
        *   @param  dml_sql -       optional parameter that indicates the data manipulation language
        *                           statement in a subquery
        *   @param  where_clause -  optional parameter, the last part of the subquery indicating a where-condition
        *
        *   @return res_asc - the info about the word, as well as the word
        */
        private function getDBWord($tbl_name, $dml_sq = "", $where_clause = ""){

            //acquire the largest and smallest number designated as IDs from the import table
            $res_asc = null;

            $mm_q_str = "SELECT MAX(ID) AS max_id, MIN(ID) AS min_id FROM $dml_sq $tbl_name $where_clause";

            $id_q = $this->db->query($mm_q_str);
            if(!$id_q)
                throw new Exception("Database Error: " . mysqli_error($this->db));

            $id_range = $id_q->fetch_object();

            //find a random number within that min and max ID range and selects a word with that ID

            do{
                $ran_num = rand($id_range->min_id, $id_range->max_id);
            } while ($this->db->query("SELECT EXISTS(SELECT * FROM $tbl_name WHERE ID = $ran_num) AS id_true")->fetch_object()->id_true == 0);

            $res_q_str = "SELECT $tbl_name.*, `Words`.Word FROM $tbl_name INNER JOIN Words ON $tbl_name.ID = $ran_num AND Words.ID = $ran_num LIMIT 0, 1";

            //query result is placed into an associative array
            $res_asc = $this->db->query($res_q_str)->fetch_assoc();
            if(!$res_asc)
                throw new Exception("Database Error: " . $this->db->errno);

            return $res_asc;
        }

        /*
        *   Checks to see if the word can be pluralized and, if so, will make the word plural
        *
        *   @param  word - the word to pluralize
        *   @param  method - the method to pluralize the word (e.g. y-ies means "remove the ending 'y' and add 'ies'")
        *
        *   @return final_word - the word in it's plural form
        */
        private function checkAndPluralize($word, $method){
            $is_split = false;
            $str_arr = array();

            //if the word does have a plural form
            if($method != "NAN" || $method != "NA" || $method != NULL || $method != ""){

                //if the word is a compound noun (eg. mother-in-law, passer-by, etc.)
                if(strpos($method, "pi_") !== FALSE){

                    //remove the pi_ from the plural method string, split the word by hyphen
                    //and grab the word in front of the first hyphen in the compound noun; that's
                    //where the plural will be appended to
                    $is_split = true;
                    $method = str_replace("pi_", "", $method);
                    $str_arr = explode("-", $word);
                    $word = $str_arr[0];

                    //remove the first word from the hyphen-split array because it's already in final_word var
                    $str_arr = array_slice($str_arr, 1);
                }

                $is_pl = false;

                switch ($method[0]) {
                    case '#':
                        $word = str_replace($method[0], '', $method);
                        $is_pl = true;
                        break;
                    case '_':
                        $method = str_replace($method[0], '', $method);
                        $word = $word . $word[strlen($word) - 1] . $method;
                        $is_pl = true;
                        break;
                    default:
                        break;
                }

                //if the plural method contains a '-'
                if(strpos($method, "-")){
                    //split the plural method by the hyphen
                    $pl_mth = explode("-", $method);

                    //remove the substring in the word that corresponds to the first element in the pl_mth Array
                    //and replace it with the second element in the pl_mth array
                    try{
                        $word = (new StringManipulator)->str_sreplace($word, $pl_mth[0], $pl_mth[1]);
                    }catch(Exception $e){echo $e->getMessage() . "<br />";}
                }
                //if it doesn't contain a '-' then simply append the plural method to the word
                else{
                    if($is_pl == false)
                        $word = $word . $method;
                }

                //if the word is a compound word then attach the rest of the hyphen-split words in the str_arr
                //behind the final word to get the true final word
                $word = (!$is_split ? $word : $word . "-" . (new StringManipulator)->arr_append($str_arr, "-"));
            }

            return $word;
        }

        //Testing Function: Simply prints out objects
        private function cn_info_printer($cn_info, $tbl_name){
            echo "<br />==========================================================================<br />";
            echo "Var Dump for $tbl_name Info: ";
            var_dump($cn_info);
            echo "<br />==========================================================================<br />";
        }
    }
?>
