<?php
/*
* Created by Ali Khalil
* Date: 9/02/2017
*/
    include 'StringManipulator.php';

    $file = null;
    $pathname = null;
    $filename = null;
    $ext_dot_loc = 0;
    $plur_spec = null;
    $plur_method = null;
    $is_plural_inner = 0;

    $files = array();
    $replacer = new StringManipulator();

    $time_start = microtime(true);

    //Checking the files in resources dir
    foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator('..\\..\\resources')) as $fileInfo){
        $is_plural_inner = 0;

        //if the file is readable
        if($fileInfo->isReadable()){

            //get the path name and determine the location of the extension seperator
            $pathname = $fileInfo->getPathname();
            $filename = $fileInfo->getFilename();
            $ext_dot_loc = strpos($filename, ".");

            //if the file is a "common noun" file and isn't a non-replaceable plural common noun file
            if(substr($filename, 0, 13) == "_common_nouns" && strpos($filename, "non_replaceable") === false){

                //if it's a "who" type common noun file...
                if(substr($filename, 13, 4) == "_wh_"){

                    //if it is a "plural inner" type common noun pluralify specification between the 30th and the extension
                    //seperator
                    if(strpos($filename, "plural_inner")){
                        $plur_spec = substr($filename, 30, $ext_dot_loc - 30);
                        $is_plural_inner = 1;
                        $str_plural_split = array();
                    }

                    //then get the pluralify specification between the 17th character and the extension seperator
                    else
                        $plur_spec = substr($filename, 17, $ext_dot_loc - 17);
                }

                //if it isn't a "who" type common noun file pluralify specification between the 14th and the extension seperator
                else
                    $plur_spec = substr($filename, 14, $ext_dot_loc - 14);

                //open the file and replace each line with the plural using the pluralify method, exit if the file can't be opened
                $file = fopen($pathname, "r");
                if(!$file)
                    exit("Error opening and reading file");

                while(!feof($file)){
                    $str = trim(fgets($file));

                    //if there is a "-" in the plural specification...
                    if(strpos($plur_spec, "-") != 0){

                        //if this is an plural_inner word then split the word by the hyphens
                        if($is_plural_inner){
                            $str_plural_split = explode("-", $str);
                        }

                        //split the singular form and the plural form as per the pluralify specification
                        $plur_method = explode("-", $plur_spec);

                        //if this is a plural_inner word then use the first part of the word to apply the plural to and then concat the
                        //rest of the parts to the end
                        if($str != '' && $is_plural_inner)
                            echo "String: " . $replacer->str_sreplace($str_plural_split[0], $plur_method[0], $plur_method[1]) . "-" . $str_plural_split[1] . "-" .$str_plural_split[2] . "<br />";

                        //if it isn't then simply use the whole word to apply the plural to
                        elseif($str != '' && !$is_plural_inner)
                            echo "String: " . $replacer->str_sreplace($str, $plur_method[0], $plur_method[1]) . "<br />";
                    }

                    //if the plural is to simply be appended at the end of the string with no removals then simply concat the plural
                    else{
                        if(!$is_plural_inner){
                            $str = $str . $plur_spec;
                            echo "String: " . $str . "<br />";
                        }

                        //if this an inner plural file then split the word by hyphens, place the plural after the first word and then
                        //attach the rest of the words at the end of the first word, seperatred by hyphens
                        else{
                            $str_plural_split = explode("-", $str);
                            $str = $str_plural_split[0] . $plur_spec;

                            for($i = 1; $i < count($str_plural_split); $i++){
                                $str = $str . "-";
                                $str = $str . $str_plural_split[$i];
                            }

                            $str = $str . "<br />";
                            echo "String: " . $str;
                        }
                    }
                }
            }
        }
    }

    echo "<br /> Total execution time of the script was: " . round((microtime(true) - $time_start) * 1000, 2) . "ms <br />";
?>
