<?php

    namespace TwitterBot\Models;
    /*
    * Created by Ali Khalil
    * Date: 10/02/2017
    */
    class StringManipulator {

        /*
        * Replace the characters at the end of a string with another set of characters
        *
        * @param    subject - the string to be acted upon
        * @param    remove  - the subset of characters to remove
        * @param    replace - the set of characters to replace the removed
        *
        * @return   subject - the subject with the replacing characters
        */
        public function str_sreplace($subject, $remove, $replace){
            //the length of the subject, the start of where we want to remove
            //and the length of the replacement
            $len_subject = strlen($subject);
            $len_replace = strlen($replace);
            $len_remove = strlen($remove);

            if($len_remove == 0 || $len_replace == 0 || $len_subject == 0)
                throw new Exception("Please ensure the strings have at least one character");
            if($len_remove < $len_subject)
                throw new Exception("Please ensure the subject string is longer than the substring to remove");

            $sub_len_remove = $len_subject - strlen($remove);

            //from the start of the removal site to the end of the word
            //make the elements empty
            for ($i = $sub_len_remove; $i < $len_subject; $i++) {
                $subject[$i] = '';
            }

            //concat the replacement to the subject to make the plural
            $subject = $subject . $replace;

            return $subject;
        }

        /*
        * Place the elements of an array into one string, seperated by the seperator
        *
        * @param    arr     - the array whose elements to string along
        * @param    septr   - the character(s) to seperate each element by
        *
        * @return   str     - a string with all the elements appended, each seperated by the seperator
        */
        public function arr_append($arr, $septr){

            //the number of elements in the array and an empty string to start with
            $arr_size = count($arr);

            if($arr_size <= 0) throw new Exception("Make sure the array is not empty");
            if($septr === '') throw new Exception("Please ensure that the seperator consists of at least on character");

            $str = "";

            //from the start until the end of the array, take the element and
            //append it to the string
            for($i = 0; $i < $arr_size; $i++){
                $str = $str . $arr[$i];

                //if this isn't the last element of the array, place the seperator
                //at the end of the appended element to seperate it and the next
                //element
                if($i + 1 < $arr_size){
                    $str = $str . $septr;
                }
            }

            return $str;
        }

        /*
        * Isolates the characters in the enclosure and returns it
        *
        * @param    str     - the string which the characters to be isolated
        *
        * @return   retStr  - the isolated substring
        */
        public function rm_from_enclose($str){
            $retStr = "";
            $s_len = strlen($str);

            if($s_len < 3) throw new Exception("Make sure the string has enclosing characters (like these parentheses) and some characters in between to isolate");

            for($i = 1; $i < $s_len - 1; $i++){
                $retStr = $retStr . $str[$i];
            }

            return $retStr;
        }
    }
?>
