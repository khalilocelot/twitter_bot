<?php

    namespace TwitterBot;

    include 'Validators\DatabaseDest.php';

    use Validators\DatabaseDest;

    //require 'DatabaseDest.php';
    //require 'LineConstructor.php';
    //require 'codebird\\codebird.php';

    $filename_w = '..\\..\\resources\\words.txt';
    $filename_b = '..\\..\\resources\\format_bnf.txt';

    $dest = new Validators\DatabaseDest();
    $line_cons = new Validators\LineConstructor();

    $file_s = fopen($filename_w, "r");

    if(!$file_s){
        echo "The file could not be opened <br />";
        exit();
    }

    while(($line = trim(fgets($file_s))) != null){
        $line_arr = explode('|', $line);
        $dest->categorize($line_arr);
    }

    fclose($file_s);

    $file_s = fopen($filename_b, "r");

    if(!$file_s){
        echo "This file could not be opened <br />";
        exit();
    }

    $bnf_array = array();

    while(($line = trim(fgets($file_s))) != null){
        array_push($bnf_array, $line);
    }

    $arr_count = count($bnf_array);
    $line_cons->recursionStart($bnf_array);

    fclose($file_s);

?>
