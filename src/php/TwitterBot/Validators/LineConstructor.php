<?php
    namespace TwitterBot\Validators;

    use TwitterBot\QuerySenders\GrammarTables as GrammarTables;
    use TwitterBot\Models\StringManipulator as StringManipulator;

    //require 'GrammarTables.php';
    //require 'StringManipulator.php';

    class LineConstructor{

        /*
        *   Kick starts the recursion function below, give it some imports
        *
        *   @param  arr - the array which contains the different line formats
        *
        *   @return NA
        */
        public function recursionStart($arr){
            $ret_str = "";
            //$this->recursion($arr[rand(0, count($arr) - 1)], $ret_str);
            $this->recursion($arr[3], $ret_str, new GrammarTable());
        }

        /*
        *   Recursively brakes down the any inner format-lines, anything within () in the bnf file
        *
        *   @param  line        - the selected above that will be used as a format to produce a line
        *   @param  ret_str     - the string to be appended to and returned
        *
        *   @return ret_str     - the string formed by following the format
        */
        private function recursion($line, $ret_str, $gr_t){
            //splits the line by whitespace between words except within brackets and
            //places them in an array
            $s_arr = preg_split("/\(\s\);?|\{\s\}|\[\s\]|[\s\c?]+(?![^{]*\})/", $line);

            //for every word in the line
            foreach($s_arr as $value){
                $val_len = strlen($value);
                echo "Value: " . $value . "<br /><br />";
                //if the element has two ways of being implemented split down the slash and
                //either select the first or last implementation to work with

                if(strpos($value, '/')){
                    $value = (($ran_num = rand(0, 1)) == 0 ? explode('/', $value)[$ran_num] . ')' : '(' . explode('/', $value)[$ran_num]);
                    $val_len = strlen($value);
                }
                else{
                    $value = $value;
                }

                //if the element is surrounded in curly brackets
                if($value[0] == "{" && $value[$val_len - 1] == "}"){
                    $this->gr_t->parseGrammar((new StringManipulator())->rm_from_enclose($value));
                }
                //if the element is surround by normal parentheses
                elseif($value[0] == "(" && $value[$val_len - 1] == ")"){
                    //recursively brake down the elements within the parens
                    $ret_str = $this->recursion((new StringManipulator())->rm_from_enclose($value), $ret_str, $gr_t);
                }
                //if the element is surrounded by square brackets
                elseif($value[0] == "[" && $value[$val_len - 1] == "]"){
                    //it's an integer block, shove a random integer at the end
                    $ret_str = $ret_str . " " . rand(0, 1000);
                }
                //if the element isn't bracketed
                else{
                    //append the element to the back of the return line
                    $ret_str = $ret_str . " " . $value;
                }
            }

            return $ret_str;
        }
    }
?>
