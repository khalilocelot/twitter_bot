<?php
/*
* Created by Ali Khalil
* Date: 9/02/2017
*/
    namespace TwitterBot\QuerySenders\Database;

    abstract class Database{
        protected $db;

        /*
        * Purpose: To establish a connection with the database
        * Imports: None
        * Exports: None
        */
        public function __construct(){

            $servername = "";
            $username = "";
            $password = "";
            $dbName = "";

            // Create connection
            $this->db = new mysqli($servername, $username, $password, $dbName);

            // Confirm connection
            if($this->db->connect_errno)
                die("Connection failed: " . $this->db->connect_error);
        }

        /*
        * Purpose: To disestablish a connection with the database
        * Imports: None
        * Exports: None
        */
        public function __destruct(){
            $this->db->close();
        }
    }
?>
