<?php
    $test_str_1 = "super pi_";
    $test_str_2 = "super_pi_";
    $test_str_3 = "super_pi_e";
    $test_str_4 = "super_pi_pi_";
    $test_str_5 = "super_pi-pie";

    $time_start = microtime(true);
    echo "String Positioning of Test 1: " . strpos($test_str_1, "pi_") . "<br/>";
    echo "Execution time: " . (microtime(true) - $time_start) . "s <br />";

    $time_start = microtime(true);
    echo "String Positioning of Test 2: " . strpos($test_str_2, "pi_") . "<br/>";
    echo "Execution time: " . (microtime(true) - $time_start) . "s <br />";

    $time_start = microtime(true);
    echo "String Positioning of Test 3: " . strpos($test_str_3, "pi_") . "<br/>";
    echo "Execution time: " . (microtime(true) - $time_start) . "s <br />";

    $time_start = microtime(true);
    echo "String Positioning of Test 4: " . strpos($test_str_4, "pi_") . "<br/>";
    echo "Execution time: " . (microtime(true) - $time_start) . "s <br />";

    $time_start = microtime(true);
    echo "String Positioning of Test 5: " . strpos($test_str_5, "pi_") . "<br/>";
    echo "Execution time: " . (microtime(true) - $time_start) . "s <br />";
?>
