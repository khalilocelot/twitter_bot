insert into `cnountypes` (`Type`) values
	('Who'),
    ('What');

insert into `PNounTypes` (`Type`) values
	('Name'),
    ('Place');

insert into `VerbsTypes` (`Type`) values 
	('Action'),
	('Transitive'),
	('Intransitive'),
    ('Primary Auxiliary Be'),
    ('Primary Auxiliary Do'),
    ('Primary Auxiliary Have'),
	('Modal Auxiliary'),
	('Stative');

insert into `DescriptiveTypes` (`Type`) values 
	('Articles'),
	('Possessive'),
	('Demonstrative'),
	('Attributes');

insert into `prepositiontypes` (`Type`) values
	('Agent'),
    ('Direction'),
    ('Place'),
    ('Time');
    